/* Mario Augusto Mota Martins */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
struct DadosUsuario{
      int chave;
      char nome[20];
      int idade;
	  int apt;
};
typedef struct DadosUsuario *data;

int TAMANHO_arquivo2 = 11;
int MAXIMO_VALOR_APONTADOR = 10;

void testeInserting(FILE *file, data *value){
	
	(*value)->chave = 1;
	strcpy((*value)->nome,"namespace1");
	(*value)->idade = 2;
	(*value)->apt = 2;
	
	data value2;
	value2=(data)malloc(sizeof(struct DadosUsuario));
	value2->chave = 2;
	strcpy(value2->nome,"namespace2");
	value2->idade = 3;
	value2->apt = 1;
	
	fwrite((*value), sizeof(struct DadosUsuario),1,file);
	fwrite(value2, sizeof(struct DadosUsuario),1,file);
}

void printStruct(data *value){
	
	printf("\nChave: %d\nNome: %s\nIdade: %d\nApontador: %d\n", (*value)->chave, (*value)->nome, (*value)->idade, (*value)->apt);
}

void testeReading(FILE *file, data *value){
	
	fseek(file, sizeof(struct DadosUsuario)*1,SEEK_SET);
	
	fread((*value), sizeof(struct DadosUsuario),1,file);
	printStruct(value);
	data value2;
	
	/* value2=(data)malloc(sizeof(struct DadosUsuario));
	fread(value2, sizeof(struct DadosUsuario),1,file);
	printStruct(&value2); */
	printf("\n");
}

void main(){
	int x,y,z;
	
	FILE *file;
	data value;
	value=(data)malloc(sizeof(struct DadosUsuario));
	
	/* file = fopen("arquivo2.dat", "r+b");
	if (file == NULL){	
		file = fopen("arquivo2.dat", "w+b");
		testeInserting(file,&value);
		fclose(file);
		file = fopen("arquivo2.dat", "r+b");
	} */
	file = fopen("arquivo2.dat", "w+b");
	testeInserting(file,&value);
	fclose(file);
	file = fopen("arquivo2.dat", "r+b");
	
	testeReading(file,&value);
	/* int i;
	scanf("%d",&i);
	printf("%d",i); */
	
	system("pause");
}