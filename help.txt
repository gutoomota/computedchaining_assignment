How Do I Compile My C Program?

To compile C program first.c, and create an executable file called first, enter:
$ gcc first.c -o first

OR
$ cc first.c -o first

To execute program first, enter:
$ ./first

or

$ first.exe

To make a txt input

$ first.exe < input.txt > output.txt