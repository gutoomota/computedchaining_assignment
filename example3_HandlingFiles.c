#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
	FILE *fp;
	char str[8];
	int vlrI;
	float vlrF;
	fp = fopen("data.bin", "r+b");

	if (fp == NULL) {
		printf("data.bin does not exist, please check!\n");
		fp = fopen("data.bin", "w+b");
		fprintf(fp, "Ricardo 30 1.740000");
		printf("bla\n");
		fclose(fp);
		fp = fopen("data.bin", "r+b");
	}
	fscanf(fp,"%s %d %f", &str, &vlrI, &vlrF);
	fclose(fp);
	printf("Value is: %s %d %f\n", str, vlrI, vlrF);
	system("pause");

	return 0;
}