/* Mario Augusto Mota Martins */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
 
struct NoReal{
      float num;
      int id; /*valor inteiro usado apenas para identificar cada nota*/
      struct NoReal *prox,*ant,*Aprox,*Aant,*Sprox,*Sant,*Dprox,*Dant,*Cprox,*Cant;
};
 
struct NoTxt{
      char txt[30];
      struct NoTxt *prox;
      struct NoReal *Nprox;
};
 
struct NoSexo{
      char sexo;
      struct NoSexo *prox;
      struct NoReal *Nprox;
};
 
typedef struct NoReal *val;
typedef struct NoTxt *txt;
typedef struct NoSexo *id;
/*funcao de inicializacaio de estruturas do tipo struct NoReal*/
void inicializaNota(val *aux){
    (*aux)=(val)malloc(sizeof(struct NoReal));
    (*aux)->id=0;
    (*aux)->ant=NULL;
    (*aux)->prox=NULL;
    (*aux)->Aant=NULL;
    (*aux)->Aprox=NULL;
    (*aux)->Sant=NULL;
    (*aux)->Sprox=NULL;
    (*aux)->Dant=NULL;
    (*aux)->Dprox=NULL;
    (*aux)->Cant=NULL;
    (*aux)->Cprox=NULL;
}
/*funcao de inicializacaio de estruturas do tipo struct NoTxt*/
void inicializaTxt(txt *aux){
    (*aux)=(txt)malloc(sizeof(struct NoTxt));
    (*aux)->prox=NULL;
    (*aux)->Nprox=NULL;
}
/*funcao de inicializacaio de estruturas do tipo struct NoSexo*/
void inicializaSexo(id *aux){
    (*aux)=(id)malloc(sizeof(struct NoSexo));
    (*aux)->sexo='f';
    (*aux)->Nprox=NULL;
    (*aux)->prox=(id)malloc(sizeof(struct NoSexo));
    (*aux)->prox->prox=NULL;
    (*aux)->prox->Nprox=NULL;
    (*aux)->prox->sexo='m';
}
/*funcao usada para inserir ordenadamente nas listas do tipo txt (alunos,disciplinas e cursos) */
void insereTxt(char valor[30],txt *lista){
    txt busca,aux;
 
    inicializaTxt(&aux);
    if (((*lista)->prox)&&(strcmp((*lista)->txt,valor)>0)){
        strcpy(aux->txt,valor);
        aux->prox=(*lista);
        (*lista)=aux;
    }else{
        for(busca=(*lista);(busca->prox)&&(strcmp(busca->prox->txt,valor)<0);busca=busca->prox);
        if (busca->prox){
            strcpy(aux->txt,valor);
            aux->prox=busca->prox;
            busca->prox=aux;
        }else{
            strcpy(busca->txt,valor);
            busca->prox=aux;
        }
    }
}
/*funcao usada para inserir notas em todas as listas que sao de sua competencia*/
void insereNota(float nota,val *notas,txt *aluno,id *sexo,txt *disciplina,txt *curso){
    val busca,Xnota;
 
    for(Xnota=(*notas);(Xnota->prox);Xnota=Xnota->prox);
    inicializaNota(&(Xnota->prox));
    Xnota->num=nota;
    Xnota->prox->id=(Xnota->id)+1;
    Xnota->prox->ant=Xnota;
    if ((*aluno)->Nprox){
        for(busca=(*aluno)->Nprox;(busca->Aprox);busca=busca->Aprox);
        busca->Aprox=Xnota;
        Xnota->Aant=busca;
    }else
        (*aluno)->Nprox=Xnota;
    if ((*sexo)->Nprox){
        for(busca=(*sexo)->Nprox;(busca->Sprox);busca=busca->Sprox);
        busca->Sprox=Xnota;
        Xnota->Sant=busca;
    }else
        (*sexo)->Nprox=Xnota;
    if ((*disciplina)->Nprox){
        for(busca=(*disciplina)->Nprox;(busca->Dprox);busca=busca->Dprox);
        busca->Dprox=Xnota;
        Xnota->Dant=busca;
    }else
        (*disciplina)->Nprox=Xnota;
    if ((*curso)->Nprox){
        for(busca=(*curso)->Nprox;(busca->Cprox);busca=busca->Cprox);
        busca->Cprox=Xnota;
        Xnota->Cant=busca;
    }else
        (*curso)->Nprox=Xnota;
}
/*funcao de busca de estrutura em uma lista do tipo struct NoTxt*/
txt buscaTxt(char valor[30],txt *lista){
    txt busca;
 
    for (busca=(*lista);(busca)&&(strcmp(busca->txt,valor)!=0);busca=busca->prox);
    return busca;
}
/*funcao de busca de estrutura em uma lista do tipo struct NoSexo*/
id buscaSexo(char aux,id *lista){
    id busca;
 
    for (busca=(*lista);(busca)&&(busca->sexo!=aux);busca=busca->prox);
    return busca;
}
/*funcao de media das notas da lista alunos*/
float mediaNotas_A(val *notas){
    int i;
    float media;
    val busca;
 
    i=0;
    media=0;
    for(busca=(*notas);(busca);busca=busca->Aprox){
        i++;
        media=media+(busca->num);
    }
    if (i)
        media=media/i;
    return media;
}
/*funcao de media das notas da lista disciplinas*/
float mediaNotas_D(val *notas){
    int i;
    float media;
    val busca;
 
    i=0;
    media=0;
    for(busca=(*notas);(busca);busca=busca->Dprox){
        i++;
        media=media+(busca->num);
    }
    if (i)
        media=media/i;
    return media;
}
/*funcao de media das notas da lista cursos*/
float mediaNotas_C(val *notas){
    int i;
    float media;
    val busca;
 
    i=0;
    media=0;
    for(busca=(*notas);(busca);busca=busca->Cprox){
        i++;
        media=media+(busca->num);
    }
    if (i)
        media=media/i;
    return media;
}
/*funcao de media das notas da lista sexo*/
float mediaNotas_S(val *notas){
    int i;
    float media;
    val busca;
 
    i=0;
    media=0;
    for(busca=(*notas);(busca);busca=busca->Sprox){
        i++;
        media=media+(busca->num);
    }
    if (i)
        media=media/i;
    return media;
}
/*funcao de media de todas as notas*/
float mediaNotas_T(val *notas){
    int i;
    float media;
    val busca;
 
    i=0;
    media=0;
    for(busca=(*notas);(busca->prox);busca=busca->prox){
        i++;
        media=media+(busca->num);
    }
    if (i)
        media=media/i;
    return media;
}
/*funcao de consulta de nota de aluno em determinada disciplina*/
float consultaNota(val *Naluno,val *Ndisc){
    val aluno,disc;
 
    aluno=*Naluno;
    while (aluno){
        disc=*Ndisc;
        while (disc){
            if (disc->id==aluno->id)
                return disc->num;
            disc=disc->Dprox;
        }
        aluno=aluno->Aprox;
    }
    return 11;
}
/*listagem de estruturas de listas do tipo struct NoTxt*/
void listagem(txt *lista){
    txt busca;
 
    busca=(*lista);
    while (busca->prox){
        printf("%s\n",busca->txt);
        busca=busca->prox;
    }
}
/*funcao para encontrar posicao de determinada nota a ser usada na funcao de remocao de notas*/
val encontraNota(val *notas,val *Naluno,val *Nsexo,val *Ndisciplina,val *Ncurso){
    val sex,aluno,disc,curso;
 
    sex=*Nsexo;
    while (sex){
        aluno=*Naluno;
        while (aluno){
            disc=*Ndisciplina;
            while (disc){
                curso=*Ncurso;
                while (curso){
                    if ((curso->id==disc->id)&&(disc->id==aluno->id)&&(aluno->id==sex->id)){
                        return curso;
                    }
                    curso=curso->Cprox;
                }
                disc=disc->Dprox;
            }
            aluno=aluno->Aprox;
        }
        sex=sex->Sprox;
    }
    return NULL;
}
/*funcao de remocao de notas*/
void remocaoNota(val *notas,val *Naluno,val *Nsexo,val *Ndisciplina,val *Ncurso){
    val busca,aux;
 
    busca=encontraNota(&*notas,&*Naluno,&*Nsexo,&*Ndisciplina,&*Ncurso);
    if (busca){
        if (busca->ant){
            aux=busca->ant;
            aux->prox=busca->prox;
            if (busca->Sant){
                aux=busca->Sant;
                aux->Sprox=busca->Sprox;
            }else
                *Nsexo=(*Nsexo)->Sprox;
            if (busca->Aant){
                aux=busca->Aant;
                aux->Aprox=busca->Aprox;
            }else
                *Naluno=(*Naluno)->Aprox;
            if (busca->Dant){
                aux=busca->Dant;
                aux->Dprox=busca->Dprox;
            }else
                *Ndisciplina=(*Ndisciplina)->Dprox;
            if (busca->Cant){
                aux=busca->Cant;
                aux->Cprox=busca->Cprox;
            }else
                *Ncurso=(*Ncurso)->Cprox;
            free(busca);
        }else{
            aux=(*notas);
            (*notas)=(*notas)->prox;
            *Nsexo=(*Nsexo)->Sprox;
            *Naluno=(*Naluno)->Aprox;
            *Ndisciplina=(*Ndisciplina)->Dprox;
            *Ncurso=(*Ncurso)->Cprox;
            free(aux);
        }
    }
}
 
int main(){
    char opcao,nome[30];
    float Xnota;
    val notas;
    txt alunos,disciplinas,cursos,Xaluno,Xdisciplina,Xcurso;
    id sexo,Xsexo;
 
    inicializaTxt(&alunos);/*inicializacao de novas listas*/
    inicializaTxt(&disciplinas);
    inicializaTxt(&cursos);
    inicializaNota(&notas);
    inicializaSexo(&sexo);
    opcao='0';
    while (opcao!='e'){/*menu principal*/
        scanf("%c",&opcao);
        getchar();
        switch(opcao){
            case 'i':/*submenu de insercao*/
                scanf("%c",&opcao);
                getchar();
                switch(opcao){
                    case 'a':/*submenu de insercao de alunos*/
                        scanf("%s",nome);
                        getchar();
                        insereTxt(nome,&alunos);
                        break;
                    case 'd':/*submenu de insercao de disciplinas*/
                        scanf("%s",nome);
                        getchar();
                        insereTxt(nome,&disciplinas);
                        break;
                    case 'c':/*submenu de insercao de cursos*/
                        scanf("%s",nome);
                        getchar();
                        insereTxt(nome,&cursos);
                        break;
                    case 'n':/*submenu de insercao de notas*/
                        scanf("%s",nome);
                        getchar();
                        Xaluno=buscaTxt(nome,&alunos);
                        scanf("%c",&opcao);
                        getchar();
                        Xsexo=buscaSexo(opcao,&sexo);
                        scanf("%s",nome);
                        getchar();
                        Xdisciplina=buscaTxt(nome,&disciplinas);
                        scanf("%s",nome);
                        getchar();
                        Xcurso=buscaTxt(nome,&cursos);
                        scanf("%f",&Xnota);
                        getchar();
                        insereNota(Xnota,&notas,&Xaluno,&Xsexo,&Xdisciplina,&Xcurso);
                        break;
                }
                break;
            case 'm':/*submenu de medias*/
                scanf("%c",&opcao);
                getchar();
                switch(opcao){
                    case 'a':/*submenu de medias de alunos*/
                        scanf("%s",nome);
                        getchar();
                        Xaluno=buscaTxt(nome,&alunos);
                        printf("%.1f\n",mediaNotas_A(&(Xaluno->Nprox)));
                        break;
                    case 'd':/*submenu de medias de disciplinas*/
                        scanf("%s",nome);
                        getchar();
                        Xdisciplina=buscaTxt(nome,&disciplinas);
                        printf("%.1f\n",(mediaNotas_D(&(Xdisciplina->Nprox))));
                        break;
                    case 'c':/*submenu de medias de cursos*/
                        scanf("%s",nome);
                        getchar();
                        Xcurso=buscaTxt(nome,&cursos);
                        printf("%.1f\n",mediaNotas_C(&(Xcurso->Nprox)));
                        break;
                    case 's':/*submenu de medias de sexo*/
                        scanf("%c",&opcao);
                        getchar();
                        Xsexo=buscaSexo(opcao,&sexo);
                        printf("%.1f\n",mediaNotas_S(&(Xsexo->Nprox)));
                        break;
                    case 't':/*submenu de media total*/
                        printf("%.1f\n",mediaNotas_T(&notas));
                        break;
                }
                break;
            case 'n':/*submenu de consulta de notas de alunos em determinada disciplina*/
                scanf("%s",nome);
                getchar();
                Xaluno=buscaTxt(nome,&alunos);
                scanf("%s",nome);
                getchar();
                Xdisciplina=buscaTxt(nome,&disciplinas);
                Xnota=consultaNota(&(Xaluno->Nprox),&(Xdisciplina->Nprox));
                if (Xnota<=10)
                    printf("%.1f\n",Xnota);
                else
                    printf("-\n");
                break;
            case 'l':/*submenu de listagem de listas do tipo struct NoTxt*/
                scanf("%c",&opcao);
                getchar();
                switch(opcao){
                    case 'a':
                        listagem(&alunos);
                        break;
                    case 'd':
                        listagem(&disciplinas);
                        break;
                    case 'c':
                        listagem(&cursos);
                        break;
                }
                break;
            case 'r':/*submenu de remocao de notas*/
                scanf("%s",nome);
                getchar();
                Xaluno=buscaTxt(nome,&alunos);
                scanf("%c",&opcao);
                getchar();
                Xsexo=buscaSexo(opcao,&sexo);
                scanf("%s",nome);
                getchar();
                Xdisciplina=buscaTxt(nome,&disciplinas);
                scanf("%s",nome);
                getchar();
                Xcurso=buscaTxt(nome,&cursos);
                remocaoNota(&notas,&(Xaluno->Nprox),&(Xsexo->Nprox),&(Xdisciplina->Nprox),&(Xcurso->Nprox));
                break;
            case 'e':/*submenu de saida do sistema*/
                break;
        }
    }
    return 1;
}