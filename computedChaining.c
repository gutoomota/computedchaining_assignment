/* Mario Augusto Mota Martins */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
struct DadosUsuario{
      int chave;
      char nome[20];
      int idade;
	  int apt;
};
typedef struct DadosUsuario *data;

int TAMANHO_ARQUIVO = 11;
int MAXIMO_VALOR_APONTADOR = 10;

int h1(int chave);
int h2(int chave);
void initialize(FILE *file, data *value);
void copyRegistro(data *valueOut, data *valueIn);
void reorderingSuccessors(FILE *file, data *value, int pos1);
void removeRegistro (FILE *file, data *value, int chave);
void insereRegistro (FILE *file, data *value, int chave, char nome[20], int idade);
void printFile(FILE *file, data *value);
int consultaRegistro (FILE *file, data *value, int chave);
void mediaAcessos(FILE *file, data *value);

int main(){
	FILE *file;
	data value;
	char input,nome[20];
	int chave,idade;
	
	initialize(file,&value);
	
	while(1){
		scanf("%c", &input);
		switch (input){
			case 'i':
				scanf("%d", &chave);
				scanf (" %[^\n]s", &nome);
				scanf("%d", &idade);
				insereRegistro(file,&value,chave,nome,idade);
				break;
			case 'c':
				scanf("%d", &chave);
				consultaRegistro(file,&value,chave);
				printf("chave: %d\n%s\n%d\n", chave, value->nome, value->idade);
				break;
			case 'r':
				scanf("%d", &chave);
				removeRegistro(file,&value,chave);
				break;
			case 'p':
				printFile(file,&value);
				break;
			case 'm':
				mediaAcessos(file,&value);
				break;
			case 'e':
				return 0;
		}
	}
	return 0;
}

int h1(int chave){
	return chave % TAMANHO_ARQUIVO;
}

int h2(int chave){
	int i = chave / TAMANHO_ARQUIVO;
	if (i==0)
		return 1;
	else
		return i;
}

void initialize(FILE *file, data *value){
	(*value) = (data)malloc(sizeof(struct DadosUsuario));
	
	file = fopen("arquivo.dat", "r+b");
	if (file == NULL){	
		(*value)->chave = -1;
		strcpy((*value)->nome,"0");
		(*value)->idade = -1;
		(*value)->apt = 0;
		file = fopen("arquivo.dat", "w+b");
		int i;
		for (i=0;i<TAMANHO_ARQUIVO;i++)
			fwrite((*value), sizeof(struct DadosUsuario),1,file);
	}
	fclose(file);
}

void copyRegistro(data *valueOut, data *valueIn){
	(*valueOut)->chave=(*valueIn)->chave;
	strcpy((*valueOut)->nome,(*valueIn)->nome);
	(*valueOut)->idade=(*valueIn)->idade;
}

void reorderingSuccessors(FILE *file, data *value, int pos1){
	int pos2, posAnt=-1;
	
	data value2 = (data)malloc(sizeof(struct DadosUsuario));
	value2->chave = -1;
	strcpy(value2->nome,"0");
	value2->idade = -1;
	value2->apt = 0;
	
	while(1){
		fseek(file, sizeof(struct DadosUsuario)*pos1,SEEK_SET);
		fread((*value), sizeof(struct DadosUsuario),1,file);
		
		if((*value)->apt != 0){
			fseek(file, sizeof(struct DadosUsuario)*pos1,SEEK_SET);
			fwrite(value2, sizeof(struct DadosUsuario),1,file);
			
			posAnt=pos1;
			pos2 = pos1+(h2((*value)->chave)*(*value)->apt);
			pos1 = h1(pos2);
			
			insereRegistro(file,&*value,(*value)->chave,(*value)->nome,(*value)->idade);
			
		}else{
			
			if(posAnt!=-1){
				fseek(file, sizeof(struct DadosUsuario)*posAnt,SEEK_SET);
				fread(value2, sizeof(struct DadosUsuario),1,file);
				value2->apt = 0;
				fseek(file, sizeof(struct DadosUsuario)*posAnt,SEEK_SET);
				fwrite(value2, sizeof(struct DadosUsuario),1,file);
			}
			
			fseek(file, sizeof(struct DadosUsuario)*pos1,SEEK_SET);
			fwrite(value2, sizeof(struct DadosUsuario),1,file);
			
			insereRegistro(file,&*value,(*value)->chave,(*value)->nome,(*value)->idade);
			break;
		}
	}
}

void removeRegistro (FILE *file, data *value, int chave){
	int posAnt =-1, pos1 = h1(chave), pos2;
	file = fopen("arquivo.dat", "r+b");
	data value2 = (data)malloc(sizeof(struct DadosUsuario));
	
	while(1){
		//busca pos de hash1
		fseek(file, sizeof(struct DadosUsuario)*pos1,SEEK_SET);
		//lê posicao de hash1
		fread((*value), sizeof(struct DadosUsuario),1,file);
		
		if ((*value)->chave == chave){
			
			if(posAnt!=-1){
				fseek(file, sizeof(struct DadosUsuario)*posAnt,SEEK_SET);
				fread(value2, sizeof(struct DadosUsuario),1,file);
				value2->apt = 0;
				fseek(file, sizeof(struct DadosUsuario)*posAnt,SEEK_SET);
				fwrite(value2, sizeof(struct DadosUsuario),1,file);
			}
			
			pos2 = pos1+(h2((*value)->chave)*(*value)->apt);
			
			(*value)->chave = -1;
			strcpy((*value)->nome,"0");
			(*value)->idade = -1;
			(*value)->apt = 0;
			fseek(file, sizeof(struct DadosUsuario)*pos1,SEEK_SET);
			fwrite((*value), sizeof(struct DadosUsuario),1,file);
			
			if((*value)->apt != 0){
				pos1 = h1(pos2);			
				reorderingSuccessors(file, &*value, pos1);
			}
			break;
		}else if((*value)->apt == 0){
			//checa se registro está vazio e insere se sim
			printf("chave nao encontrada: %d\n", chave);
			break;
		}else{
			posAnt = pos1;
			pos2 = pos1+(h2((*value)->chave)*(*value)->apt);
			pos1 = h1(pos2);
		}
	}
	fclose(file);
}

void insereRegistro (FILE *file, data *value, int chave, char nome[20], int idade){
	int posAnt, hash2, aptCount=0, pos1 = h1(chave), pos2;
	file = fopen("arquivo.dat", "r+b");
	data value2 = (data)malloc(sizeof(struct DadosUsuario));
	
	//busca pos de hash1
	fseek(file, sizeof(struct DadosUsuario)*pos1,SEEK_SET);
	//lê posicao de hash1
	fread((*value), sizeof(struct DadosUsuario),1,file);
	
	if(((*value)->chave != -1)&&((*value)->chave != chave)&&(h1((*value)->chave)!=pos1)){
			
		data listaSucessores[MAXIMO_VALOR_APONTADOR-1];
		int count = 0;
		
		listaSucessores[count] = (data)malloc(sizeof(struct DadosUsuario));
		copyRegistro(&listaSucessores[count],&*value);
		
		removeRegistro (file, &value2, (*value)->chave);
		insereRegistro(file, &value2, chave, nome, idade);
		
		value2->chave = -1;
		strcpy(value2->nome,"0");
		value2->idade = -1;
		value2->apt = 0;
		while((*value)->apt!=0){
			count++;
			pos2 = pos1+(h2((*value)->chave)*(*value)->apt);
			pos1 = h1(pos2);
			
			fseek(file, sizeof(struct DadosUsuario)*pos1,SEEK_SET);
			fread((*value), sizeof(struct DadosUsuario),1,file);
			
			listaSucessores[count] = (data)malloc(sizeof(struct DadosUsuario));
			copyRegistro(&listaSucessores[count],&*value);
			
			fseek(file, sizeof(struct DadosUsuario)*pos1,SEEK_SET);
			fwrite(value2, sizeof(struct DadosUsuario),1,file);
			
			fseek(file, sizeof(struct DadosUsuario)*1,SEEK_SET);
		}
		int i;
		for(i=0;i<=count;i++){
			insereRegistro(file, &value2, listaSucessores[i]->chave, listaSucessores[i]->nome, listaSucessores[i]->idade);
		}
	}else while(1){
		
		if ((*value)->chave == chave){
			printf("chave ja existente: %d\n", chave);
			break;
			
		}else if((*value)->chave == -1){
			//checa se registro está vazio e insere se sim
			if (aptCount>0){
				fseek(file, sizeof(struct DadosUsuario)*posAnt,SEEK_SET);
				value2->apt = aptCount;
				fwrite(value2, sizeof(struct DadosUsuario),1,file);
			}
			fseek(file, sizeof(struct DadosUsuario)*pos1,SEEK_SET);
				
			(*value)->chave = chave;
			strcpy((*value)->nome,nome);
			(*value)->idade = idade;
			(*value)->apt = 0;
			
			fwrite((*value), sizeof(struct DadosUsuario),1,file);
			break;
		}else if(aptCount>0){
			pos2 = pos1+hash2;
			pos1 = h1(pos2);
			aptCount++;
			if (aptCount>MAXIMO_VALOR_APONTADOR)
				break;
		}else{
			if ((*value)->apt==0){
				copyRegistro(&value2,&*value);
				
				posAnt = pos1;
				hash2 = h2((*value)->chave);
				pos2 = pos1+hash2;
				aptCount++;
				if (aptCount>MAXIMO_VALOR_APONTADOR)
					break;
			}else
				pos2 = pos1+(h2((*value)->chave)*(*value)->apt);
			pos1 = h1(pos2);
		}
		//busca pos de hash1
		fseek(file, sizeof(struct DadosUsuario)*pos1,SEEK_SET);
		//lê posicao de hash1
		fread((*value), sizeof(struct DadosUsuario),1,file);
	}
	fclose(file);
}

void printFile(FILE *file, data *value){
	file = fopen("arquivo.dat", "r+b");
	fseek(file, sizeof(struct DadosUsuario)*0,SEEK_SET);
	int i;
	for (i=0;i<TAMANHO_ARQUIVO;i++){
		fread((*value), sizeof(struct DadosUsuario),1,file);
		if((*value)->chave == -1)
			printf("%d: vazio\n", i);
		else if((*value)->apt == 0)
			printf("%d: %d %s %d nulo\n", i, (*value)->chave, (*value)->nome, (*value)->idade);
		else
			printf("%d: %d %s %d %d\n", i, (*value)->chave, (*value)->nome, (*value)->idade, (*value)->apt);
	}
	fclose(file);
}

int consultaRegistro (FILE *file, data *value, int chave){
	int accessCounter=0, pos1 = h1(chave), pos2;
	file = fopen("arquivo.dat", "r+b");
	while(1){
		accessCounter++;
		//busca pos de hash1
		fseek(file, sizeof(struct DadosUsuario)*pos1,SEEK_SET);
		//lê posicao de hash1
		fread((*value), sizeof(struct DadosUsuario),1,file);
		
		if ((*value)->chave == chave)
			return accessCounter;
		else if((*value)->apt == 0){
			//checa se registro está vazio e insere se sim
			printf("chave nao encontrada: %d\n", chave);
			return 0;
		}else{
			pos2 = pos1+(h2((*value)->chave)*(*value)->apt);
			pos1 = h1(pos2);
		}
	}
	fclose(file);
}

void mediaAcessos(FILE *file, data *value){
	int totalRecords=0,totalAccess=0;
	float media;
	file = fopen("arquivo.dat", "r+b");
	fseek(file, sizeof(struct DadosUsuario)*0,SEEK_SET);
	
	int i;
	for (i=0;i<TAMANHO_ARQUIVO;i++){
		fread((*value), sizeof(struct DadosUsuario),1,file);
		if((*value)->chave != -1){
			totalRecords++;
			totalAccess += consultaRegistro (file, &*value, (*value)->chave);
		}
	}
	media = totalAccess/(float)totalRecords;
	printf("%.1f\n", media);
	
	fclose(file);
}
